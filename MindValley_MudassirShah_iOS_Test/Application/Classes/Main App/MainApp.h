//
//  MainApp.h
//  MindValley_MudassirShah_iOS_Test
//
//  Created by Mudassir Shah on 9/4/16.
//  Copyright © 2016 Mind Valley. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainApp : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

