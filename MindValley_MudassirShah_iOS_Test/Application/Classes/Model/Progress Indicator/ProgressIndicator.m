//
//  ProgressIndicator.m
//  MindValley_MudassirShah_iOS_Test
//
//  Created by Mudassir Shah on 9/4/16.
//  Copyright © 2016 Mind Valley. All rights reserved.
//

#import "ProgressIndicator.h"
#import "MBProgressHUD.h"

@interface ProgressIndicator()
{
     MBProgressHUD *hud;
}
@end

@implementation ProgressIndicator
-(instancetype) init
{
    self = [super init];
    if(self)
    {
        
    }
    return self;
}


-(void) ShowAngularWithText : (NSString *) text OnView : (UIViewController *)vc
{
    hud = [MBProgressHUD showHUDAddedTo:vc.view animated:YES];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = text;
}

-(void) ShowDeterminateWithText : (NSString *) text OnView : (UIViewController *)vc
{
    hud = [MBProgressHUD showHUDAddedTo:vc.view animated:YES];
    hud.mode = MBProgressHUDModeDeterminate;
    hud.labelText = text;
}
-(void) ShowDimBackground : (NSString *) text OnView : (UIViewController *)vc
{
    hud = [MBProgressHUD showHUDAddedTo:vc.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.dimBackground  = YES;
    hud.labelText = text;
}

-(void) HideOnView : (UIViewController *)vc
{
    [MBProgressHUD hideHUDForView:vc.view animated:YES];
}
-(void) HideAfterSeconds : (float)seconds
{
    [hud hide:YES afterDelay:seconds];
}
-(void) Hide
{
    [hud hide:YES];
}

@end
