//
//  ProgressIndicator.h
//  MindValley_MudassirShah_iOS_Test
//
//  Created by Mudassir Shah on 9/4/16.
//  Copyright © 2016 Mind Valley. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ProgressIndicator : NSObject
{
   
}
-(void) ShowAngularWithText : (NSString *) text OnView : (UIViewController *)vc;
-(void) ShowDimBackground : (NSString *) text OnView : (UIViewController *)vc;

-(void) ShowDeterminateWithText : (NSString *) text OnView : (UIViewController *)vc;
-(void) HideOnView : (UIViewController *)vc;
-(void) HideAfterSeconds : (float)seconds;
-(void) Hide;
@end
