//
//  FileManager.m
//  SSD
//
//  Created by Mudassir Shah on 7/6/16.
//  Copyright © 2016 Ilmasoft. All rights reserved.
//

#import "FileManager.h"

@interface FileManager()
{
    NSString * documentsPath,* documentsDirectoryPath;
    NSArray *paths;
}
@property (strong,nonatomic) NSFileManager * fileManager;

@end

@implementation FileManager

-(instancetype)init
{
    self = [super init];
    if(self)
    {
        self.fileManager = [NSFileManager defaultManager];
        paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectoryPath = [[NSString alloc] initWithString:[paths objectAtIndex:0]];
        
    }
    return self;
}

-(BOOL)SaveFilewithName:(NSString *)FileName  andData : (NSData *) data
{
    @try
    {
        documentsPath = [documentsDirectoryPath stringByAppendingPathComponent:FileName];
        //documentsPath = [NSString stringWithFormat:@"./%@",documentsPath];
        if([self.fileManager fileExistsAtPath:documentsPath])
        {
    
            if([data writeToFile:documentsPath options:kNilOptions error:nil])
            {
                NSLog(@"Existing File '%@' is Written Successfully", FileName);
                return  YES;
            }
            else
            {
                return  NO;
            }
        }
        else
        {
            if([self.fileManager createFileAtPath:documentsPath contents:data attributes:nil])
            {
                if([self.fileManager fileExistsAtPath:documentsPath])
                {
                    NSLog(@"New File '%@' Created and Written Successfully", FileName);

                    return  YES;
                }
                else
                {
                    return NO;
                }
            }
            else
            {
                return NO;
            }

        }
        
        
    }
    @catch (NSException *exception)
    {
        return NO;
    }
    
    
}

-(BOOL)SaveFilewithName:(NSString *)FileName  andArray : (NSArray *) array
{
    @try
    {
        if(array != nil && array.count != 0)
        {
            documentsPath = [documentsDirectoryPath stringByAppendingPathComponent:FileName];
            if([self.fileManager fileExistsAtPath:documentsPath])
            {
                if([array writeToFile:documentsPath atomically:YES])
                {
                    NSLog(@"Existing File '%@' is Written Successfully", FileName);

                    return  YES;
                }
                else
                {
                    return NO;
                }
            }
            else
            {
                if([self.fileManager createFileAtPath:documentsPath contents:nil attributes:nil])
                {
                    if([self.fileManager fileExistsAtPath:documentsPath])
                    {
                        if([array writeToFile:documentsPath atomically:YES])
                        {
                            NSLog(@"New File '%@' Created and Written Successfully", FileName);

                            return  YES;
                        }
                        else
                        {
                            return NO;
                        }
                    }
                    else
                    {
                        return NO;
                    }
                }
                else
                {
                    return NO;
                }
            }
            
        }
        else
        {
            return NO;
        }
        
    }
    @catch (NSException *exception)
    {
        return NO;
    }
    
    
}

-(NSData *) LoadDataWithFileName:(NSString *)FileName
{
    @try
    {
        documentsPath = [documentsDirectoryPath stringByAppendingPathComponent:FileName];
        
        NSData * data;
        if([self.fileManager fileExistsAtPath:documentsPath])
        {
            
            data = [[NSData alloc] initWithContentsOfFile:documentsPath];
            if(data != nil)
            {
                NSLog(@"File '%@' Read Succesfully.", FileName);
            }
            return data;
            
        }
        else
        {
            NSLog(@"File '%@' does not exist.", FileName);

            return nil;
            
        }
        
    }
    @catch (NSException *exception)
    {
        return nil;
    }
    
}


-(NSArray *) LoadArrayWithFileName:(NSString *)FileName
{
    @try
    {
        documentsPath = [documentsDirectoryPath stringByAppendingPathComponent:FileName];
        
        NSArray * array;
        if([self.fileManager fileExistsAtPath:documentsPath])
        {
            array = [[NSArray alloc] initWithContentsOfFile:documentsPath];
            if(array != nil)
            {
                NSLog(@"File '%@' Read Succesfully.", FileName);
            }
            return array;
        }
        else
        {
            NSLog(@"File '%@' does not exist.", FileName);

            return nil;
        }
        
    }
    @catch (NSException *exception)
    {
        return nil;
    }
    
}


-(NSData *)LoadDataWithFileNameFromAppResources : (NSString *)FileName withFileExtension : (NSString *)Type
{
    NSString * mb = [[NSBundle mainBundle] pathForResource:FileName ofType:Type];
    return [NSData dataWithContentsOfFile:mb];

}

-(NSString *)LoadStringWithFileNameFromAppResources : (NSString *)FileName withFileExtension : (NSString *)Type
{
    NSString * mb = [[NSBundle mainBundle] pathForResource:FileName ofType:Type];
    return [NSString stringWithContentsOfFile:mb encoding:NSUTF8StringEncoding error:nil];
    
}

-(NSArray *)LoadArrayWithFileNameFromAppResources : (NSString *)FileName withFileExtension : (NSString *)Type
{
    NSString * mb = [[NSBundle mainBundle] pathForResource:FileName ofType:Type];
    return [NSArray arrayWithContentsOfFile:mb];
    
}
-(NSString *)GetPath
{
    return  documentsPath;
}

-(BOOL)CreateDirectoryWithName :(NSString *)name
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:[documentsDirectoryPath stringByAppendingPathComponent:name]])
    {
        
        NSError* error;
        if([[NSFileManager defaultManager] createDirectoryAtPath:[documentsDirectoryPath stringByAppendingPathComponent:name] withIntermediateDirectories:NO attributes:nil error:&error])
        {
            NSLog(@"Directory Created with Name: %@", name);
            return YES;
        }
        else
        {
            NSLog(@"Directory not created, reason unknown.");
            return NO;
        }
    }
    else
    {
        NSLog(@"Directory not created, already exists.");

        return NO;
    }
}
@end
