//
//  FileManager.h
//  SSD
//
//  Created by Mudassir Shah on 7/6/16.
//  Copyright © 2016 Ilmasoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileManager : NSObject
{
   
}

-(BOOL)SaveFilewithName:(NSString *)FileName  andData : (NSData *) data;
-(BOOL)SaveFilewithName:(NSString *)FileName  andArray : (NSArray *) array;
-(NSData *) LoadDataWithFileName:(NSString *)FileName;
-(NSArray *) LoadArrayWithFileName:(NSString *)FileName;
-(NSData *)LoadDataWithFileNameFromAppResources : (NSString *)FileName withFileExtension : (NSString *)Type;
-(NSString *)LoadStringWithFileNameFromAppResources : (NSString *)FileName withFileExtension : (NSString *)Type;
-(NSArray *)LoadArrayWithFileNameFromAppResources : (NSString *)FileName withFileExtension : (NSString *)Type;
-(NSString *)GetPath;
-(BOOL)CreateDirectoryWithName :(NSString *)name;

@end
