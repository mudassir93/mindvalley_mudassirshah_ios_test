//
//  WebService.m
//  MindValley_MudassirShah_iOS_Test
//
//  Created by Mudassir Shah on 9/4/16.
//  Copyright © 2016 Mind Valley. All rights reserved.
//

#import "WebService.h"

@implementation WebRequest

-(instancetype)init
{
    self = [super init];
    if(self)
    {
    }
    return self;
}

-(void)getDataFromURL:(NSURL *)url withCompletionHandler:(void (^)(NSData *))completionHandler andTimeOut : (float)TimeOut
{
    
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        // Instantiate a session configuration object.
    
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
        sessionConfig.timeoutIntervalForRequest = TimeOut;
    
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
        // Instantiate a session object.
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
        // Create a data task object to perform the data downloading.
        NSURLSessionDataTask *task = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
        {
        
            if (error != nil)
            {
                // If any error occurs then just display its description on the console.
                NSLog(@"%@", [error localizedDescription]);
            }
            else
            {
                // If no error occurs, check the HTTP status code.
                NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
            
                // If it's other than 200, then show it on the console.
                if (HTTPStatusCode != 200)
                {
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^
                     {
                         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                         completionHandler(nil);
                     }];
                    NSLog(@"HTTP status code = %ld", (long)HTTPStatusCode);
                }
                else
                {
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^
                     {
                         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                         completionHandler(data);
                     }];
                }
                
             }
        }];
    
        [task resume];
    }
    

@end
