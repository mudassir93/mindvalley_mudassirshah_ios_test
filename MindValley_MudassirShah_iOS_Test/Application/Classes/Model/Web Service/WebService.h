//
//  WebService.h
//  MindValley_MudassirShah_iOS_Test
//
//  Created by Mudassir Shah on 9/4/16.
//  Copyright © 2016 Mind Valley. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface WebRequest : NSObject
{
    
}
-(void)getDataFromURL:(NSURL *)url withCompletionHandler:(void(^)(NSData *data))completionHandler andTimeOut : (float)TimeOut;

@end
