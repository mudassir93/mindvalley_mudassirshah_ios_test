//
//  Alert.m
//  MindValley_MudassirShah_iOS_Test
//
//  Created by Mudassir Shah on 9/4/16.
//  Copyright © 2016 Mind Valley. All rights reserved.
//


#import "Alert.h"
#import "Global.h"
#import <CRToast/CRToast.h>
#import "SCLAlertView.h"

@interface Alert()
{
    UIAlertController * AC;
    SCLAlertView * SCLAV;
    BOOL ToastStatus;
}
@end

@implementation Alert

-(instancetype)init
{
    self = [super init];
    if(self)
    {
        
    }
    return self;
}

-(void)ShowAlertWithMessage : (NSString *) Message onViewController : (UIViewController *)vc
{
    AC = [UIAlertController alertControllerWithTitle:AppName
                                             message:Message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [AC addAction:defaultAction];
    [vc presentViewController:AC animated:YES completion:nil];
    
}
    // Present the alert view controller
-(void)ShowAlertWithMessage : (NSString *) Message andTitle : (NSString *) Title onViewController : (UIViewController *)vc
{
    AC = [UIAlertController alertControllerWithTitle:Title
                                             message:Message
                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [AC addAction:defaultAction];
    [vc presentViewController:AC animated:YES completion:nil];
}


-(void)ShowToastWithMessage : (NSString *) Message andTitle : (NSString *)title andTextAlignment : (NSTextAlignment)alignment andBackgroundColor : (UIColor *) bgColor  withIcon : (NSString *)image
{
    NSMutableDictionary *options = [@{kCRToastNotificationTypeKey               : @(CRToastTypeNavigationBar),
                                      kCRToastNotificationPresentationTypeKey   : @(CRToastPresentationTypeCover),
                                      kCRToastUnderStatusBarKey                 : @(NO),
                                      kCRToastTextKey                           : title,
                                      kCRToastTextAlignmentKey                  : @(alignment),
                                      kCRToastTimeIntervalKey                   : @(3),
                                      kCRToastAnimationInTypeKey                : @(CRToastAnimationTypeGravity),
                                      kCRToastAnimationOutTypeKey               : @(CRToastAnimationTypeGravity),
                                      kCRToastAnimationInDirectionKey           : @(0),
                                      kCRToastAnimationOutDirectionKey          : @(0),
                                      kCRToastBackgroundColorKey                : bgColor
                                      
                                      } mutableCopy];
    
    options[kCRToastImageKey] = [UIImage imageNamed:image];
    options[kCRToastImageAlignmentKey] = @(CRToastAccessoryViewAlignmentLeft);
    options[kCRToastSubtitleTextKey] = Message;
    options[kCRToastSubtitleTextAlignmentKey] = @(alignment);
    options[kCRToastSubtitleTextAlignmentKey] = @(alignment);
    options[kCRToastInteractionRespondersKey] = @[[CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeTap
                                                                                                  automaticallyDismiss:YES
                                                                                                                 block:^(CRToastInteractionType interactionType)
    {
        
    }]];

    if(!ToastStatus)
    {
        [CRToastManager showNotificationWithOptions:[NSDictionary dictionaryWithDictionary:options]
                                 apperanceBlock:^(void)
         {
             ToastStatus = YES;
             NSLog(@"Appeared");
     
         }
                                    completionBlock:^(void)
         {
             ToastStatus = NO;
             NSLog(@"Completed");
        
         }];
    }
        
}

-(void)ShowErrorAlertWithMessage : (NSString*) Message andTitle :(NSString *)Title onViewController : (UIViewController *)view
{
    SCLAV = [[SCLAlertView alloc] init];
    [SCLAV showError:view title:Title subTitle:Message closeButtonTitle:@"Ok" duration:0.0];
}

-(void)ShowSuccessAlertWithMessage : (NSString*) Message andTitle :(NSString *)Title onViewController : (UIViewController *)view
{
    SCLAV = [[SCLAlertView alloc] init];
    [SCLAV showSuccess:view title:Title subTitle:Message closeButtonTitle:@"Ok" duration:0.0];
}

-(void)ShowWarningAlertWithMessage : (NSString*) Message andTitle :(NSString *)Title onViewController : (UIViewController *)view
{
    SCLAV = [[SCLAlertView alloc] init];
    [SCLAV showWarning:view title:Title subTitle:Message closeButtonTitle:@"Ok" duration:0.0];
}

-(void)ShowInfoAlertWithMessage : (NSString*) Message andTitle :(NSString *)Title onViewController : (UIViewController *)view
{
    SCLAV = [[SCLAlertView alloc] init];
    [SCLAV showInfo:view title:Title subTitle:Message closeButtonTitle:@"Ok" duration:0.0];
}
@end
