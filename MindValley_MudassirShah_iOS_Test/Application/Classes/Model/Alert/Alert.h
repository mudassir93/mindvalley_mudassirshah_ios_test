//
//  Alert.h
//  MindValley_MudassirShah_iOS_Test
//
//  Created by Mudassir Shah on 9/4/16.
//  Copyright © 2016 Mind Valley. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIAlertController.h>

@interface Alert : NSObject 
{
   
}
-(void)ShowAlertWithMessage : (NSString *) Message onViewController : (UIViewController *)vc;
-(void)ShowAlertWithMessage : (NSString *) Message andTitle : (NSString *) Title onViewController : (UIViewController *)vc;
-(void)ShowToastWithMessage : (NSString *) Message andTitle : (NSString *)title andTextAlignment : (NSTextAlignment)alignment andBackgroundColor : (UIColor *) bgColor  withIcon : (NSString *)image;
-(void)ShowErrorAlertWithMessage : (NSString*) Message andTitle :(NSString *)Title onViewController : (UIViewController *)view;
-(void)ShowSuccessAlertWithMessage : (NSString*) Message andTitle :(NSString *)Title onViewController : (UIViewController *)view;
-(void)ShowWarningAlertWithMessage : (NSString*) Message andTitle :(NSString *)Title onViewController : (UIViewController *)view;
-(void)ShowInfoAlertWithMessage : (NSString*) Message andTitle :(NSString *)Title onViewController : (UIViewController *)view;


@end
