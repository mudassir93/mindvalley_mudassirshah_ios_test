//
//  JSONParser.h
//  MindValley_MudassirShah_iOS_Test
//
//  Created by Mudassir Shah on 9/4/16.
//  Copyright © 2016 Mind Valley. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONParser : NSObject
{
    
}
-(NSDictionary *)GetDictionaryFromJSONData : (NSData *) data;
-(NSData * )GetJSONDataFromDictionary : (NSDictionary *) Dictionary;
-(NSString * )GetJSONStringFromDictionary : (NSDictionary *) Dictionary;

@end
