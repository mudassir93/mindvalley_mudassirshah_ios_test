//
//  JSONParser.m
//  MindValley_MudassirShah_iOS_Test
//
//  Created by Mudassir Shah on 9/4/16.
//  Copyright © 2016 Mind Valley. All rights reserved.
//


#import "JSONParser.h"

@interface JSONParser()

@end

@implementation JSONParser

-(instancetype) init
{
    self = [super init];
    if(self)
    {
        
    }
    return  self;
}
-(NSDictionary *)GetDictionaryFromJSONData : (NSData *) data;
{
    NSError *error;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                options:kNilOptions
                                                  error:&error];
    return  dictionary;
}



-(NSData * )GetJSONDataFromDictionary : (NSMutableDictionary *) Dictionary
{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:Dictionary options:0 error:&err];
    
    return  jsonData;
}

-(NSString * )GetJSONStringFromDictionary : (NSMutableDictionary *) Dictionary
{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:Dictionary options:0 error:&err];
    NSString * jsonString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    return  jsonString;
}
                               
@end
