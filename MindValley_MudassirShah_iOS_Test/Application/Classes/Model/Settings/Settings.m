//
//  Settings.m
//  MindValley_MudassirShah_iOS_Test
//
//  Created by Mudassir Shah on 9/4/16.
//  Copyright © 2016 Mind Valley. All rights reserved.
//

#import "Settings.h"

@interface Settings()
{
    NSUserDefaults * udef;
}
@end

@implementation Settings

-(instancetype) init
{
    self = [super init];
    if(self)
    {
        udef = [NSUserDefaults standardUserDefaults];
    }
    return  self;
}


-(NSObject *) LoadObjectWithKey : (NSString *)key
{
    @try
    {
        if([udef objectForKey:key] != nil)
        {
            return [udef objectForKey:key];
        }
        else
        {
            return  nil;
        }
    }
    @catch (NSException *exception)
    {
        return  nil;
    }

}

-(BOOL) SaveObject : (NSObject * ) value WithKey : (NSString *) key
{
    @try
    {
        [udef setObject:value forKey:key];
        [udef synchronize];
        return true;
    }
    @catch (NSException *exception)
    {
        return false;
    }
}


-(BOOL) LoadBooleanWithKey : (NSString *)key
{
    @try
    {
        if([udef stringForKey:key] != nil && ![[udef stringForKey:key] isEqualToString:@""])
        {
            return [udef boolForKey:key];
        }
        else
        {
            return  NO;
        }
    }
    @catch (NSException *exception)
    {
        return  NO;
    }
    
}

-(BOOL) SaveBoolean : (BOOL) value WithKey : (NSString *) key
{
    @try
    {
        [udef setBool:value forKey:key];
        [udef synchronize];
        return true;
    }
    @catch (NSException *exception)
    {
        return false;
    }
}

-(NSString *) LoadStringWithKey : (NSString *)key
{
    @try
    {
        if([udef stringForKey:key] != nil && ![[udef stringForKey:key] isEqualToString:@""])
        {
            return [udef stringForKey:key];
        }
        else
        {
            return  nil;
        }
    }
    @catch (NSException *exception)
    {
        return  nil;
    }
    
}

@end
