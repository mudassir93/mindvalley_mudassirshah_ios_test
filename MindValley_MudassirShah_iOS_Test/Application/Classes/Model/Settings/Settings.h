//
//  Settings.h
//  MindValley_MudassirShah_iOS_Test
//
//  Created by Mudassir Shah on 9/4/16.
//  Copyright © 2016 Mind Valley. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface Settings : NSObject
{
}

-(NSObject *) LoadObjectWithKey : (NSString *)key;
-(BOOL) LoadBooleanWithKey : (NSString *)key;
-(NSString *) LoadStringWithKey : (NSString *)key;
-(BOOL) SaveBoolean : (BOOL) value WithKey : (NSString *) key;
-(BOOL) SaveObject : (NSObject * ) value WithKey : (NSString *) key;

@end
